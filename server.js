const express = require('express')
const app = express()
const PORT = 3000

// Serve file html for client
// app.get('/chat', function (req, res) {
//     res.sendFile(__dirname + '/public/index.html')
// })

// app.use(express.static('public'))

app.use(express.static('public'))


// Về nghiên cứu serve static trong express

// require('fs') => file system
// require('path') => path
// require('http') => 
// require('https') => 

const server = require('http').createServer(app)
const io = require('socket.io')(server)

// let sockets = []
// let counter = []

io.on('connection', function (socket) {
    // counter++
    // io.emit("CHAT_INFO", { clients: counter })

    // socket.on("disconnect", function() {
    //     counter--
    //     io.emit("CHAT_INFO", { clients: counter })
    // })

    socket.on("MESSAGE", function (data) {
        data.id = socket.id
        io.sockets.emit("MESSAGE", data)
    })

    socket.on("P-MESSAGE", function (data) {
        let pid = data.id
        data.id = socket.id
        io.to(pid).emit("P-MESSAGE", data)
    })
})

server.listen(PORT, () => console.log(`Server listening on port ${PORT}...`)) // `` => literal