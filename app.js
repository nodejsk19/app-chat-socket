// var arr = [1, 2, 3, 5, 10]

// arr.length => property
// arr.reverse() => method

// console.log("RESULT:", arr.reverse())

// var string = "Hello World!"
// console.log(string.split("").reverse().join())

// METHOD & ROUTER (endpoint)

const express = require('express')
const app = express()

app.get("/hello", function (req, res) {
    console.log("Have user request")
    res.send("Hello World!")
})

// Param & Query
app.get("/hello/:name", function (req, res) {
    var name = req.params.name
    res.send("Hello " + name)
})

app.get("/calculator/:num1/:num2", (req, res) => {
    let num1 = req.params.num1
    let num2 = req.params.num2
    let result = parseInt(num1) + parseInt(num2)
    res.send("Result:" + result)
})

app.listen(3000, function () {
    console.log("Server start on port 3000...")
})